export default [
    {
      "id": 1,
      "name": "Programação de computadores I",
      "period": "1º período",
      "cht": "60",
      "knowledge_area": "Computação",
    },
    {
        "id": 2,
        "name": "Fundamentos matemáticos da computação",
        "period": "1º período",
        "cht": "60",
        "knowledge_area": "Matemática",
    },
    {
        "id": 3,
        "name": "Seminários de Sistemas de Informação",
        "period": "1º período",
        "cht": "30",
        "knowledge_area": "Humanas",
    },
    {
        "id": 4,
        "name": "Teoria da computação para Sistemas de Informação",
        "period": "1º período",
        "cht": "60",
        "knowledge_area": "Humanas",
    },
    {
        "id": 5,
        "name": "Fundamentos de Sistemas de Informação",
        "period": "1º período",
        "cht": "60",
        "knowledge_area": "Humanas",
    }
]