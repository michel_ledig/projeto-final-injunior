import React from 'react'
import './VizCurriculo.css'
import BarraLateral from '../BarraLateral/BarraLateral'
import '../Template/Template.css'
import data from '../../data.js'
import MatriculaCard from './components/MatriculaCard.jsx'


function VizCurriculo() {
    return (
        <div className="Template">
            <BarraLateral></BarraLateral>

            <div className="fundo">
                <div className="curriculo-txt">
                    <h1>Visualizar currículos</h1>
                        
                    <div className="curriculo-corpo">
                        <h2>Visualize aqui o currículo de qualquer um dos cursos da UFF.</h2>
                        <div className="caixa-seletora">
                            <h2>Curso:</h2>
                            <input type="button" id="check"></input>
                            <label id="container-check" for="check"></label>
                            
                            <div className="menu-drop">
                                
                                <div className="materias-container">
                                    <h className="materias">Exemplo</h>
                                    <h className="materias">Exemplo</h>
                                    <h className="materias">Exemplo</h>
                                </div>

                            </div>
                        </div>
                        
                        {data.map(materia => (
                           <MatriculaCard 
                                nome= {materia.name}
                                id={materia.id}
                                period={materia.period}
                                cht={materia.cht}
                                knowledge_area={materia.knowledge_area}
                           />
                        ))}

                    </div>
                </div>
                
            </div>
        </div>
        
    )
}


export default VizCurriculo