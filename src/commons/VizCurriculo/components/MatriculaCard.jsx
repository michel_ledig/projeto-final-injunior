import React from 'react'

function MatriculaCard(props) {
    return (
        <table>
            <tr>
                <th>{props.nome}</th>
                <th>{props.period}</th>
                <th>{props.cht}</th>
            </tr>
        </table>
    )
}

export default MatriculaCard
