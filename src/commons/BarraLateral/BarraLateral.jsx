import React from 'react'
import './BarraLateral.css'

function BarraLateral() {
    return (
        <div className="barra-aluno">
            <div className="dados-aluno">
                <h2 className="dados-txt">Fulano da Silva</h2>
                <h2 className="dados-txt">Sistemas de Informação</h2>
                <h2 className="dados-txt">M. 01301000123</h2>
                <div className="botao-dados">
                    <button>Editar Dados</button>
                </div>
            </div>
            <div className="separador"></div>
            <div className="opcoes">
                <h3 className="opcoes-txt">Histórico</h3>
                <h3 className="opcoes-txt">Currículos</h3>
                <h3 className="opcoes-txt">Turmas</h3>
                <h3 className="opcoes-txt">Inscrição Online</h3>
            </div>

        </div>
    )
}

export default BarraLateral