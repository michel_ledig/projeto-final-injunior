import './App.css';
import Login from './pages/Login/Login.jsx'
import VizCurriculo from './commons/VizCurriculo/VizCurriculo'
import Curriculos from './pages/Aluno/Curriculos/Curriculos'


function App() {
  return (
    <div className="App">
      <VizCurriculo/>
    </div>
  );
}

export default App;
