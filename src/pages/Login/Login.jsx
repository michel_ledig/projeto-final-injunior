import React from 'react'
import LoginCard from './components/LoginCard/LoginCard.jsx'
import LoginText from './components/LoginText/LoginText.jsx'
import './Login.css'

function Login() {
    return (
        <div className="tela">
            <div className="Login">
                <div className="LoginText">  
                    <LoginText></LoginText>
                </div> 
                <div className="LoginCard">
                    <LoginCard/>
                </div>                      
            </div>
        </div>

    )
}

export default Login
