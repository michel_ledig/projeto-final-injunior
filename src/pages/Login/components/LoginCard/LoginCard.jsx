import React from 'react'
import './LoginCard.css'
import { useForm } from "react-hook-form";


export default function LoginCard() {
   
    // coletando 3 funçoes do hook useform
    const { register, handleSubmit}  = useForm();
    // declacarei a função e desestruturei o que o useForm retorna em 3 variaveis diferentes 


    //funcao pra printar dados coletado pelo handleSubmit
    const onSubmit = (data) => {
        console.log(data);
    }

    return (
        <div className="LoginCard-div">

            <h1>Acesse o seu idUFF</h1>

            <form onSubmit={handleSubmit(onSubmit)}>
                
                <label className="container-login">
                    <p className="txt-login">Seu CPF (somente números)</p>
                    <input type="text" name="CPF" ref={register}>
                    </input>
                </label>

                <label className="container-login">
                    <p className="txt-login">Senha do seu idUFF</p>
                    <input type="text" name="senha" ref={register}></input>
                </label> 

                <button id="logar"> Logar </button>
            </form>
                    
            
        </div>
    )
}
