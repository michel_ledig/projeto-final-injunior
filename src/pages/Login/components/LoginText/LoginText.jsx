import React from 'react'
import './LoginText.css'

function LoginText() {
    return (
        <div className="LoginText">  
            <h1 className="Titulo">idUFF</h1>
            <h2 className="Subtítulo">Bem vindo ao Sistema Acadêmico da Graduação</h2>
            <p className="corpo-login">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mattis ex vel mauris porta pulvinar. Integer luctus bibendum ex et faucibus. Vivamus sagittis consectetur quam et rhoncus. Cras imperdiet nisi et elementum dictum. Duis nec turpis eleifend, viverra turpis et, egestas felis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Phasellus quis posuere sem. 
            </p>
        </div>
    )
}

export default LoginText
