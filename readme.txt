#projeto-final

O reitor da uff entrou em contato com célula de vendas da IN Junior, pedindo para que a empresa refaça o sistema do idUFF, seguindo os requisitos apresentados. A célula de criação já executou o wireframe do projeto, que vocês podem conferir em https://www.figma.com/file/45ylrD1X7MZGcAmlR130KG/TREINAMENTO-Projeto-Final-ID-UFF?node-id=0%3A1

Os grupos abaixo deverão fazer a aplicação do idUFF, que atenda os requisitos, seguindo a modelagem de negócios do documento anexado. O grupo deve executar e entregar dentro do prazo:
- o layout do projeto
- o Diagrama de Entidades Relacional do banco de dados 
- gitlabs do front e do back
- deploy do front e do back em produção no Heroku. 
O prazo de entrega do projeto será sexta-feira às 23:59.


Antonio Nunes